import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/sample',
    name: 'sample',
    component: () => import('@/views/samples.vue'),
    meta: {
      title: '物流系統',
    },
  },
];

const router = new VueRouter({
  routes,
});

export default router;
